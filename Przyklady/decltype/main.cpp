struct Test
{
  Test() { m_x = 42; }
  int m_x;
};

int main()
{
  int x;
  const int cx = 42;
  const int &crx = x;
  const Test *p = new Test();

  using x_type = decltype(x);
  auto a = x;
  using cx_type = decltype(cx);
  auto b = cx;
  using crx_type = decltype(crx);
  auto c = crx;
  using m_x_type = decltype(p->m_x);
  auto d = p->m_x;

  delete p;
  return 0;
}