#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

class C
{
public:
	C() {}	//C() : m_d(0.0), m_p(nullptr), m_y{1,2,3,4} {}
private:
	double m_d = 0.0;
	char* m_p {nullptr};
	int m_y[5] {1,2,3,4};
};

struct MyStruct
{
	int x=7;
	MyStruct() = default;
	MyStruct(int y) : x(y) {}
};

int main() 
{
	int a{0};
	string s{"hello"};
	string s2{s}; //konstruktor kopiujący

	vector <string> vs{"A", "B", "C"};
	map<string, string> stars{ 
		{"Imie1", "Nazwisko1"},
	  	{"Imie2", "Nazwisko2"}
	  };
	double *pd= new double [3] {0.5, 1.2, 12.99};

	//domyślna inicjalizacja
	int n{}; 				// inicjalizacja do 0
	int *intPtr{}; 			// inicjalizacja do nullptr
	double d{}; 			// inicjalizacja do 0.0
	char cArr[12]{}; 		// wszystkie 12 znaków są zainicjalizowane do '\0'
	string str{}; 			// tak samo jak: string s;
	char *p=new char [5]{}; // wszystkie 5 znaków są zainicjalizowane do '\0'
	
	delete[] pd;
	delete[] p;

	MyStruct obj1;
	MyStruct obj2{10};
	cout << obj1.x << " " << obj2.x << endl;

	return 0;
}