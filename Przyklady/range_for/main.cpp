#include <iostream>
#include <vector>

int main()
{
    std::vector<int> v = {0, 1, 2, 3, 4, 5};

    for (const int &i : v)
        std::cout << i << ' ';
    std::cout << std::endl;

    for (auto i : v)
    {
        std::cout << i << ' ';
        i = 0;
    }
    std::cout << std::endl;

    for (auto &i : v)
    {
        std::cout << i++ << ' ';
    }
    std::cout << std::endl;

    for (const int &i : v)
        std::cout << i << ' ';
    std::cout << std::endl;

    for (int i : {10, 20, 30})
        std::cout << i << ' ';
    std::cout << std::endl;

    int tab[] = {10, 20, 30};

    for (int i : tab)
        std::cout << i << ' ';
    std::cout << std::endl;

    int *k = new int[3];

    /*
    for (int i : k) 
        std::cout << i << ' ';
    std::cout << std::endl;
    */
    delete[] k;
}