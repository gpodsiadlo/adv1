#include <stdexcept>
#include <iostream>

constexpr int binToInt(const char *string, int bit = 0, int value = 0)
{
  return *string == '\0' ? value : 
  *string == ',' ? binToInt(string + 1, bit, value) : 
  *string == '1' ? binToInt(string + 1, bit + 1, (value * 2) + 1) : 
  *string == '0' ? binToInt(string + 1, bit + 1, (value * 2)) : 
  throw std::logic_error("Only '0', '1', ',' allowed");
}

int main()
{
  constexpr int i1 = binToInt("1");
  std::cout << "1b -> " << i1 << std::endl;

  constexpr int i3 = binToInt("11");
  std::cout << "11b -> " << i3 << std::endl;

  constexpr int i19 = binToInt("10011");
  std::cout << "10011b -> " << i19 << std::endl;
  
  constexpr int i178 = binToInt("1011,0010");
  std::cout << "1011,0011 -> " << i178 << std::endl;

  return 0;
}