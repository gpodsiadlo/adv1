/* @autor: Grzegorz Podsiadło 
Małe zadanie rozgrzewkowe sprawdzające umiejętność pisania krótkich wyrażeń lambda.

UWAGA:
1.) Prosze nie dodawać żadnych plików
2.) Proszę o edycję tego pliku tylko w miejscach oznaczonych komentarzem.

CEL ZADANIA:
Celem zadania jest zapoznanie studentów z podstawowym sposobem używania wyrażeń lambda oraz nauka
używania co najmniej jednego z dwóch sposobów wywolywań rekurencyjnych z ich użyciem.
*/

/* 
 Miejsce na dodanie niezbędnych nagłówków:
*/
#include <iostream>
#include <array>
#include <algorithm>

int main()
{

  /* Proszę w tym miejscu stworzyć przy pomocy wyrażenia lambda rekurencyjna funkcję obliczającą silnię */

  std::cout << "Silnia z 5 to: " << /* wywołanie */ << std::endl;

  const std::array<int, 10> arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  double sum = 0.0;

  /* Proszę poniżej stworzyć wyrażenie sumujące zawartość tablicy do zmiennej sum oraz wypisujące na ekran jej zawartość */

  std::for_each(arr.cbegin(), arr.cend(), /* tutaj */);
  std::cout << std::endl
            << "Suma: " << sum;

  return 0;
}

/*
Silnia z 5 to: 120
1 2 3 4 5 6 7 8 9 10
Suma: 55
*/