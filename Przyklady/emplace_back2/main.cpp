﻿#include <iostream>
#include <vector>
#include <utility>

class Foo
{
public:
	//************************************************************************
	Foo() : m_i(-1), m_b(false), m_f(-1.0)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	Foo(int i, bool b, float f) : m_i(i), m_b(b), m_f(f)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	Foo(const Foo& obj) : m_i(obj.m_i), m_b(obj.m_b), m_f(obj.m_f)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	Foo(Foo&& obj) : m_i(std::move(obj.m_i)), m_b(std::move(obj.m_b)), m_f(std::move(obj.m_f))
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	~Foo()
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	
	//************************************************************************

	Foo(int i, bool b, float f, const std::vector<int>& vInt) : m_i(i), m_b(b), m_f(f), m_vInt(vInt)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	Foo(int i, bool b, float f, std::vector<int>&& vInt) : m_i(i), m_b(b), m_f(f), m_vInt(std::move(vInt))
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}

	//************************************************************************

private:
	int m_i;
	bool m_b;
	float m_f;
	std::vector<int> m_vInt;
};

class FooVector
{
public:
	FooVector()
	{
		m_v.reserve(3);
	}

	template<typename ... Args>			// szablon o zmiennej liczbie parametrow
	void AddFoo(Args&& ... args)		// forwarding reference
	{	
		m_v.emplace_back(std::forward<Args>(args)...);
	}

private:
	std::vector<Foo> m_v;
};

int main()
{
	FooVector v;
	v.AddFoo(5, true, 10.0f);
	v.AddFoo();


	std::vector<int> vecInt = {1, 2, 3, 4, 5};
	v.AddFoo(5, true, 10.0f, vecInt);
	std::cout << "Rozmiar vecInt: " << vecInt.size() << std::endl;
	return 0;
}
