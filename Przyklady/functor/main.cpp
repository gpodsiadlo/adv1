#include <iostream>
#include <algorithm>
#include <vector>

struct Summator
{
  Summator(int &sum) : _sum(sum) {}
  void operator()(int n) const { _sum += n; }
  int &_sum;
};

int main()
{
  std::vector<int> vect = {5, 4, 3, 2, 1};
  int sum = 0;
  Summator summator(sum);

  std::for_each(vect.begin(), vect.end(), summator);
  std::cout << "Sum: " << sum << std::endl;

  sum = 0;

  std::for_each(vect.begin(), vect.end(), [&sum](int i) { sum += i; });
  std::cout << "Sum: " << sum << std::endl;

  return 0;
}