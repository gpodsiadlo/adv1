#include <random>
#include <iostream>
#include <vector>
#include <string>

int main()
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::cout << "Zakres silnika: [" << mt.min() << ", " << mt.max() << ")" << std::endl;

    std::uniform_int_distribution<int> iDist(0, 10); // zakres: [0,10]
    std::cout << "uniform_int_distribution:" << std::endl;
    for (int i = 0; i < 5; ++i)
    {
        std::cout << iDist(mt) << std::endl; //uruchamiamy silnik i przepuszczamy wynik przez rozklad
    }

    std::uniform_real_distribution<double> dDist(0.0, 10.0); // zakres: [0,10.0)
    std::cout << "\nuniform_real_distribution:" << std::endl;
    for (int i = 0; i < 5; ++i)
    {
        std::cout << dDist(mt) << std::endl;
    }

    std::cout << "\nRozklad normalny:" << std::endl;
    std::normal_distribution<> nDist(5, 2); // (wartosc oczekiwana, odchylenie standardowe)
    std::vector<int> vec(10);
    for (int i = 0; i < 400; ++i)
    {
        int val = nDist(mt);
        if (val >= 0 && val <= 10)
            vec[val]++;
    }

    for (int i = 0; i < 10; ++i)
    {
        std::cout << i << ": " << std::string(vec[i], '*') << std::endl;
    }

    return 0;
}