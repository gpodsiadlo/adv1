#include <iostream>
#include <string>
#include <vector>
#include <initializer_list>

class InitClass {
public:
	// konstruktor przyjmujący listę inicjującą
	InitClass(std::initializer_list<int> list) : m_v(list) {}

	void print() const 
	{
		for (const int& i : m_v)
			std::cout << i << " ";
		std::cout << std::endl;
	}
private:
	std::vector<int> m_v;
};


int main() 
{
	InitClass obj{10, 20, 30};
	obj.print();
	return 0;
}