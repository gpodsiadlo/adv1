﻿// Proszę napisać odpowiednie klasy, aby output zgadzał się z podanym na końcu pliku main.cpp
// (oczywiście 4 ostatnie cyfry z dwóch ostatnich linijek są losowe i mogą się różnić)

// Proszę napisać szablon klasy MyRand, która pozwala generować liczby losowe korzystając 
// z podanych przez użytkownika elementów. Parametrami szablonu są kolejno typ wartości losowej,
// rodzaj generatora i rodzaj rozkładu. Do konstruktora przekazujemy zakres liczb losowych.

// MyClass przechowuje 3 zmienne typu int, bool oraz std::string. Pozwala zwrócić zestaw przechowywanych danych.

// Klasa szablonowa Wrapper przechowuje wektor elementów typu T. Posiada interfejs pozwalający dodawać elementy do 
// wektora oraz przeglądać zawartość wektora.

// Proszę nie zmieniać pliku main.cpp
// Autor: D.K.

#include "main.h"

int main()
{
	MyRand<int, std::mt19937, std::uniform_int_distribution<>> r{1, 20};

	MyClass s1{r.nextRand(), false, "Pierwsza"};
	MyClass s2{r.nextRand(), true, "Druga"};
	std::cout << "bool: " << std::get<bool>(s1.package()) << " string: " << std::get<std::string>(s1.package()) << std::endl;

	Wrapper<int> w{0, 1, 2, r.nextRand(), r.nextRand()};
	w.append( {std::get<0>(s1.package()), std::get<0>(s2.package())} );
	w.print();

	const int* ptr;
	std::size_t size;
	std::tie(ptr, size) = w.getInfo();

	for(unsigned i=0; i<size; ++i)
		std::cout << *ptr++ << " ";
	std::cout << "\n";

	return 0;
}

// bool: 0 string: Pierwsza
// Ilość elementów we Wrapperze: 5
// Ilość dodanych elementów: 2
// 0 1 2 11 5 15 13
// 0 1 2 11 5 15 13