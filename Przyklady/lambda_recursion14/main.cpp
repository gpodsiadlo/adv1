#include <iostream>
#include <functional>

int main()
{

  auto recursive_lambda_implementation = [](int d, auto &functor) -> void {
    std::cout << "Recursion depth: " << d << std::endl;
    if (d < 10)
      functor(d + 1, functor);
  };

  recursive_lambda_implementation(0, recursive_lambda_implementation);

  return 0;
}