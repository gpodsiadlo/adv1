#include <iostream>
#include <string>
#include <utility>

void funWewnetrzna(std::string&) 
{
	std::cout << "wersja std::string&" << std::endl;
}
void funWewnetrzna(std::string&&) 
{
	std::cout << "wersja std::string&&" << std::endl;
}

template<typename T>
void funZewnetrzna(T&& arg) 
{
	funWewnetrzna(std::forward<T>(arg));
}

int main() 
{
	std::string s;
	funZewnetrzna(s);	// T -> std::string&
	funZewnetrzna(std::move(s));	// T -> std::string&&

	return 0;
}