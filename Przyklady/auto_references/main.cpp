#include <iostream>

int main()
{
  int x = 10;
  int &rx = x;
  auto a = rx;

  rx = 11;
  std::cout << "x: " << x << " " << std::endl;

  a = 12;
  std::cout << "a: " << a << std::endl;
  std::cout << "x: " << x << std::endl;

  auto & ra = x;
  ra = 13;

  std::cout << "ra: " << ra << std::endl;
  std::cout << "x: " << x << std::endl;

  const auto & cra = x;

  std::cout << "cra: " << cra << std::endl;

  //cra = 14;

  return 0;
}