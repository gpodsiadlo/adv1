/* @autor: Grzegorz Podsiadło 
Celem zadania jest stworzenie kilku aliasów dla obiektów
posiadających operator(), a następnie zdefiniowanie prostej klasy
FiltersBuilder, która odpowiada za tworzenie zestawu filtrów 
używanych do odrzucania elementów kontenera, które są z tymi filtrami niezgodne.

UWAGA:
1.) Pliku main.cpp nie można modyfikować. 
2.) Słowa kluczowe typname/class mogą znajdować się tylko w
    definicji szablonowych aliasów oraz sygnaturze klasy FiltersBuilder
3.) Rozwiązanie nie powinno powodować wycieków / błędów pamięci.
4.) Wszystko co nie jest powyżej zabronione/nakazane jest dozwolone.

CEL ZADANIA:
Celem zadania jest nauka używania szablonowych aliasów typów oraz podstawowych wyrażeń lambda używających słowa kluczowego this.
*/

#include "Aliases.h"
#include "Filters.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <list>

auto STLPrinter = [](const auto &container) {
  for (const auto &el : container)
    std::cout << el << " ";
  std::cout << std::endl;
};

int main()
{
  std::vector<int> vec = {-10, 4, 5, 19, 32, 99, -44, 0, 33, -88, -33};
  std::cout << "Oryginalny wektor: " << std::endl;
  STLPrinter(vec);

  const int minimalValue = 4;
  predicateType<int> pred = [minimalValue](const int &val) { return val >= minimalValue; };

  const std::string delimiter = " ";
  mapperType<int, std::string> toStringWithDelimMapper = [delimiter](const int &val) { return std::to_string(val) + delimiter; };

  std::cout << "Wartości większe lub równe 4: " << std::endl;
  for (const auto &el : vec)
    if (pred(el))
      std::cout << toStringWithDelimMapper(el);
  std::cout << std::endl;

  comparatorType<int> comparator = std::greater<int>();
  std::sort(vec.begin(), vec.end(), comparator);

  std::cout << "Posortowany malejąco: " << std::endl;
  for (const auto &el : vec)
    std::cout << el << " ";
  std::cout << std::endl;

  /////////////////////////////////////////////////////
  std::list<int> list = {0, 11, 43, 33, 343, 332, -1, -3434, 7, 6, -43, -1, 7645, -4, -32, 55, -55};
  std::cout << "Oryginalna lista:" << std::endl;
  STLPrinter(list);

  FiltersBuilder<int> fb(-10, 10); //min, max
  fb.addIsLesserThanMinimumFilter();
  fb.addIsGreaterThanMaximumFilter();
  fb.addIsEvenFilter();

  const auto &filters = fb.getFilters();

  std::cout << "Odfiltrowywanie wartości:" << std::endl;
  for (predicateType<int> filter : filters)
  {
    list.remove_if(filter);
    STLPrinter(list);
  }

  return 0;
}

/*
Oryginalny wektor:
-10 4 5 19 32 99 -44 0 33 -88 -33
Wartości większe lub równe 4:
4 5 19 32 99 33
Posortowany malejąco:
99 33 32 19 5 4 0 -10 -33 -44 -88
Oryginalna lista:
0 11 43 33 343 332 -1 -3434 7 6 -43 -1 7645 -4 -32 55 -55
Odfiltrowywanie wartości:
0 11 43 33 343 332 -1 7 6 -1 7645 -4 55
0 -1 7 6 -1 -4
0 6 -4
*/