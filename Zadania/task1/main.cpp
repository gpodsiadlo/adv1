// Celem zadania jest napsanie szablonu klasy MySmartPointer.
// Ma on bazowac na semnatyce przenoszenia.
// Odkomentowanie linii 7 i 8 powoduje blad kompilacji.
// Proszę nie modyfikować pliku main.cpp

// Autor D.K.

#include "MySmartPointer.h"

// #define ERROR
// #define ERROR2

int main()
{
	MySmartPointer<double> smart = new double(1.1);
	std::cout << smart;

	MySmartPointer<double> smart2(std::move(smart));

	if (smart)
		std::cout << smart;
	else
		std::cout << "Pusty wskaznik" << std::endl;

#ifdef ERROR
	MySmartPointer<double> smart3(smart2);
#endif

	smart2 = new double(2.2);
	if (smart2)
		std::cout << *smart2 << std::endl;

	MySmartPointer<double> smart3(new double(3.3));

#ifdef ERROR2
	smart3 = smart2;
#endif

	smart3 = std::move(smart2);
	smart3.print();

	return 0;
}

//MySmartPointer typu: d = 1.1
//Pusty wskaznik
//2.2
//MySmartPointer = 2.2
//Zwalnianie pamieci w destruktorze!