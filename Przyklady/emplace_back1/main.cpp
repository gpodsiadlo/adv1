﻿#include <iostream>
#include <vector>
#include <utility>

class Foo
{
	friend std::ostream& operator<<(std::ostream&, const Foo&);
public:
	//************************************************************************
	Foo(int i) : m_i(i), m_b(false)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	Foo(int i, bool b) : m_i(i), m_b(b)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	Foo(const Foo& obj) : m_i(obj.m_i), m_b(obj.m_b)
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	Foo(Foo&& obj) : m_i(std::move(obj.m_i)), m_b(std::move(obj.m_b))
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	~Foo()
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}
	//************************************************************************

private:
	int m_i;
	bool m_b;
};

std::ostream& operator<<(std::ostream& out, const Foo& obj)
{
	return out << obj.m_i << " " << std::boolalpha << obj.m_b;
}

int main()
{
	std::vector<Foo> v;
	v.reserve(4);
	{
		Foo objC(5, true);
		v.push_back(objC);				//kopiowanie
	}
	std::cout << "\n";
	{
		Foo objM(5, true);
		v.push_back(std::move(objM));	//przenoszenie
	}
	std::cout << "\n";
	{
		v.push_back(Foo(5, true));		//przenoszenie
	}
	std::cout << "\n";
	{
		v.emplace_back(5, true);		//tworzenie bezpośrednio w wektorze
	}
	std::cout << "\n";

	for (const Foo& f : v)
		std::cout << f << std::endl;

	return 0;
}
