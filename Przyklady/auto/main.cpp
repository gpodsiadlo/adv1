#include <iostream>

int main()
{
  //auto wrong;
  auto a = 1 + 2;
  auto b = 3.3;
  auto c = 2 + 3.5;
  //auto d = 3, e = 5.5;
  auto f = 2, g = 3;
  std::cout << "a: " << a << " b: " << b
            << " c: " << c << " f: " << f << " g: " << g
            << std::endl;

  return 0;
}