#include <iostream>
#include <tuple>
#include <string>
#include <typeinfo>

int main()
{
	std::tuple<int, std::string, char> t(10, "przykladowy_string", 'z');

	//get() - daje dostep do elementow krotki, zwraca referencje przez co umozliwia edycje zawartosci
	std::cout << "String przed: " << std::get<1>(t) << std::endl;
	std::get<1>(t) = "nowy_string";
	std::cout << "String po: " << std::get<1>(t) << std::endl;

	std::cout << "\nInt przed: " << std::get<0>(t) << std::endl;
	int &i = std::get<0>(t);
	i++;
	std::cout << "Int po: " << std::get<0>(t) << std::endl;

	//std::get<3>(t); blad kompilacji, spoza zakresu

	std::tuple<int, std::string, char> t2;																	 //konstruktory domyslne
	t2 = std::tuple<int, std::string, char>(20, "przykladowy_string2", 'x'); //robi to samo co funkcja ponizej
	//make_tuple() - konstruuje obiekt krotki o wlasciwym typie, zdefiniowanym przez argumenty
	t2 = std::make_tuple(20, "przykladowy_string2", 'x');

	t = t2; // kopiowanie skladnik po skladaniku

	int x;
	std::string y;
	char z;
	//tie() - tworzy krotke, ktorej elementami sa referencje do podanych argumentow -> rozpakowanie krotki
	std::tie(x, y, z) = t;
	std::cout << "\n"
						<< x << " " << y << " " << z << std::endl;
	std::tie(x, std::ignore, y) = t; //stala ignore, pozwala zaniechac jeden z elementow krotki

	//tuple_cat() - tworzy krotke, ktorej typy elementow sa suma argumentow
	std::tuple<double, int> t3;
	std::tuple<char, int> t4;
	auto t5 = std::tuple_cat(t3, t4); //<double, int, char, int>

	//type traits
	std::cout << "\nTuple size: " << std::tuple_size<decltype(t5)>::value << std::endl;
	std::tuple_element<0, decltype(t5)>::type doub; // doub typu double
	std::cout << "doub typu " << typeid(doub).name() << std::endl;

	//C++ 14 - Tuple addressing via type
	//umozliwia dostep do elementow krotki za posrednictwem nazw typow (tylko dla typow jednoznacznych)

	std::tuple<int, int, std::string> t14(1, 2, "string14");
	std::cout << "\nElement krotki t14 bedacy typu string: " << std::get<std::string>(t14) << std::endl;
	// std::cout << "Pierwszy element krotki t14 typu int: " << std::get<int>(t14) << std::endl;
	return 0;
}