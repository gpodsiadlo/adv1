#include <iostream>

struct not_on_heap
{
  void *operator new(std::size_t) = delete;
  not_on_heap(int arg) : m_var(arg) {}
  not_on_heap() = default;
  void print() const
  {
    std::cout << "Argument is " << m_var << std::endl;
  }

private:
  int m_var;
};

void double_only(double arg)
{
  std::cout << "Argument is: " << arg << std::endl;
}

void double_only(int arg) = delete;

template <typename T>
void real_double_only(T) = delete;

void real_double_only(double arg)
{
  std::cout << "Argument is: " << arg << std::endl;
}

int main()
{
  double_only(10.0f);
  double_only(10.0);
  //double_only(10);

  //real_double_only(10.0f);
  real_double_only(10.0);
  //real_double_only(10);

  not_on_heap noh(10);
  noh.print();
  //not_on_heap * noh_ptr = new not_on_heap(10);

  return 0;
}