#include <iostream>
#include <memory>
#include <string>
#include <vector>

class Test
{
public:
	Test(int a = 0) : val(a) {}
	~Test() {}

	int val;
};

void Fun(std::auto_ptr<Test> p)
{
	std::cout << p->val << std::endl;
}

int main()
{
	std::auto_ptr<Test> ptr(new Test(5));
	Fun(ptr);
	// std::cout << ptr->val << std::endl;

	//std::auto_ptr<Test> p(new Test[10]); // gdy auto_ptr wychodzi poza zakres wolany jest delete (NIE delete[])
}