struct Test
{
  Test() { m_x = 42; }
  int m_x;
};

int main()
{

  int x;
  const int cx = 42;
  const int &crx = x;
  const Test *p = new Test();

  using x_with_parens_type = decltype((x));
  using x_type = decltype(x);
  auto a_p = (x);
  auto a = x;
  using cx_with_parens_type = decltype((cx));

  using cx_type = decltype(cx);

  auto b_p = (cx);
  auto b = cx;

  using crx_with_parens_type = decltype((crx));
  using crx_type = decltype(crx);

  auto c_p = (crx);
  auto c = crx;

  using m_x_with_parens_type = decltype((p->m_x));

  using m_x_type = decltype(p->m_x);

  auto d_p = (p->m_x);
  auto d = p->m_x;

  delete p;
  return 0;
}