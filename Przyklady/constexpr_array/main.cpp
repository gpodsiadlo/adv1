#include <iostream>

template <typename T, int N>
class ConstexprArray
{
  T m_data[N];

public:
  constexpr ConstexprArray() : m_data{} {}
  constexpr const T &operator[](const int index) const { return m_data[index]; }
  constexpr T &operator[](const int index) { return m_data[index]; }
  constexpr int size() const { return N; }
};

constexpr auto makeArrayWithSequence(const int start) -> ConstexprArray<int, 100>
{
  int seq = start;
  ConstexprArray<int, 100> result{};
  for (int i{}; i < 100; ++i)
    result[i] = seq++;

  return result;
}

int main()
{
  constexpr auto ar = makeArrayWithSequence(0);
  static_assert(ar[50] == 50, "Init function not working properly.");
  for (int i{}; i < ar.size(); ++i)
    std::cout << ar[i] << " ";
  std::cout << std::endl;

  return 0;
}