#include <iostream>
#include <memory>
#include <string>
#include <vector>

class SPClass
{
public:
	SPClass(const int val = -1) : m_val(val) { std::cout << "Konstruktor: " + std::to_string(m_val) << std::endl; }
	~SPClass() { std::cout << "Destruktor: " + std::to_string(m_val) << std::endl; }
	void Print() { std::cout << m_val << std::endl; }

private:
	int m_val;
};

int main()
{
	//std::unique_ptr<SPClass> sp1(new SPClass(1)); //poprawne, ale niezalecane

	std::shared_ptr<SPClass> sp1 = std::make_shared<SPClass>(1); //rekomendowane

	//inicjalizacja przez konstruktor kopiujacy, inkrementacja licznika
	std::shared_ptr<SPClass> sp2(sp1);

	//inicjalizacja przez przypisanie, inkrementacja licznika
	std::shared_ptr<SPClass> sp3 = sp2;

	//inicjalizacja wykorzystujaca nullptr, sp4 jest puste
	std::shared_ptr<SPClass> sp4(nullptr);

	//sp1 i sp2 zamieniaja zarowno wskazniki jak i liczniki
	sp1.swap(sp2);

	//dynamic_pointer_cast, static_pointer_cast i const_pointer_cast

	//recznie tworzymy dealokator
	std::shared_ptr<SPClass> sp5(new SPClass[5],
															 [](SPClass *ptr) { delete[] ptr; });

	//int* p = new int;
	//std::shared_ptr<int> sptr1(p);
	//std::shared_ptr<int> sptr2(p);
}