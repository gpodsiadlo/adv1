#include <iostream>
#include <memory>
#include <string>
#include <vector>

class WPClass
{
public:
	WPClass(const int val = -1) : m_val(val) { std::cout << "Konstruktor: " << m_val << std::endl; }
	~WPClass() { std::cout << "Destruktor: " << m_val << std::endl; }
	void Print() { std::cout << m_val << std::endl; }

private:
	int m_val;
};

class B;
class A
{
public:
	~A() { std::cout << "Destruktor A" << std::endl; }
	std::weak_ptr<B> m_spB;
};
class B
{
public:
	~B() { std::cout << "Destruktor B" << std::endl; }
	std::weak_ptr<A> m_spA;
};

int main()
{
	std::shared_ptr<WPClass> sp = std::make_shared<WPClass>();
	std::weak_ptr<WPClass> wp(sp);
	{
		std::shared_ptr<WPClass> sp1 = std::make_shared<WPClass>();

		//konstrukcja weak_ptr
		std::weak_ptr<WPClass> wp1(sp1);
		std::weak_ptr<WPClass> wp2 = wp1; //1 strong ref, 2 weak ref

		std::cout << "Strong ref: " << sp1.use_count() << std::endl;
		std::cout << "Strong ref: " << wp2.use_count() << std::endl;

		wp = sp1;
		std::cout << "wp is expired: " << wp.expired() << std::endl;
	}
	std::cout << "wp is expired: " << wp.expired() << std::endl;

	std::shared_ptr<WPClass> newSp = wp.lock(); //tworzymy shared_ptr z weak_ptr
	std::cout << "Is null: " << (nullptr == newSp.get()) << std::endl;

	//*******************************************************
	// std::shared_ptr<B> spB = std::make_shared<B>();
	// std::shared_ptr<A> spA = std::make_shared<A>();
	// std::cout << "Strong ref A: " << spA.use_count() << std::endl;
	// std::cout << "Strong ref B: " << spB.use_count() << std::endl;
	// spB->m_spA = spA;
	// spA->m_spB = spB;
	// std::cout << "Strong ref A: " << spA.use_count() << std::endl;
	// std::cout << "Strong ref B: " << spB.use_count() << std::endl;
}