#include <iostream>

// zawiera właściwą zawartość jednego składnika w krotce
template<std::size_t i, typename Item>
struct TupleLeaf {
    Item value;
};

// TupleImpl jest pośrednikiem do klasy finalnej, która ma dodatkowy 
// parametr szablonu 'i'
template<std::size_t i, typename... Items>
struct TupleImpl;

// podstawowy przypdek: pusta krotka
template<std::size_t i>
struct TupleImpl<i>{};

// rekursywna specializacja
template<std::size_t i, typename HeadItem, typename... TailItems>
struct TupleImpl<i, HeadItem, TailItems...> :
    TupleLeaf<i, HeadItem>, // dodajemy składnik 'value' typu HeadItem
    TupleImpl<i + 1, TailItems...> // rekurencja
    {};

// uzyskujemy i-tą referencję do składnika w krotce
template<std::size_t i, typename HeadItem, typename... TailItems>
HeadItem& Get(TupleImpl<i, HeadItem, TailItems...>& tuple) {
    return tuple.TupleLeaf<i, HeadItem>::value;
}

// templated alias, w celu uniknięcia wpisywania 'i=0'
template<typename... Items>
using Tuple = TupleImpl<0, Items...>;

int main() {
    Tuple<int, float, std::string> tuple;
    Get<0>(tuple) = 5;
    Get<1>(tuple) = 8.3;
    Get<2>(tuple) = "Foo";
    std::cout << Get<0>(tuple) << std::endl;
    std::cout << Get<1>(tuple) << std::endl;
    std::cout << Get<2>(tuple) << std::endl;
    return 0;
}