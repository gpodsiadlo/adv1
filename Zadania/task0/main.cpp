// Proste zadanie rozgrzewkowe związane z semantyką przenoszenia.
// Proszę napisać zestaw klas oraz funkcji, które będą odpowiednio reagować zarówno na l-wartości jak i r-wartości.
// Celem zadania jest zapoznanie się w praktyce z podstawami semantyki przenoszenia jak i rozróżnianiem l i r wartości.
// Proszę nie modyfikować pliku main.cpp

// Autor D.K.

#include "main.h"

std::string getValue()
{
	return "Zwracam";
}

int main()
{
	std::string s = "Mój string";
	std::cout << s << "\n"
						<< std::endl;

	MakeCopy c(s);
	std::cout << c.getStr() << " - " << s << "\n"
						<< std::endl;

	MakeMove m(std::move(s));
	std::cout << m.getStr() << " - " << s << "\n"
						<< std::endl;

	std::string s2 = "Drugi string";
	printReference(s2);
	printReference(std::move(s2));
	printReference(getValue());
	return 0;
}

// Mój string
//
// Dokonuję kopiowania
// Mój string - Mój string
//
// Dokonuję przenoszenia
// Mój string -
//
// l-wartość = Drugi string
// r-wartość = Drugi string
// r-wartość = Zwracam