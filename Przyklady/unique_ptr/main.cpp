#include <iostream>
#include <memory>
#include <string>
#include <vector>

class UPClass
{
public:
	UPClass(const int val = -1) : m_val(val) { std::cout << "Konstruktor: " + std::to_string(m_val) << std::endl; }
	~UPClass() { std::cout << "Destruktor: " + std::to_string(m_val) << std::endl; }
	void Print() { std::cout << m_val << std::endl; }

private:
	int m_val;
};

int main()
{

	//std::unique_ptr<UPClass> up1(new UPClass(1)); //poprawne, ale niezalecane

	//std::unique_ptr<UPClass> up1 = new UPClass(1); //niejawne użycie konstruktora
	//***************************
	//  explicit unique_ptr(pointer Ptr);
	//***************************

	std::unique_ptr<UPClass> up1 = std::make_unique<UPClass>(1); //rekomendowane, bezpieczeństwo pod względem wyjątków (C++ 14)

	{
		std::unique_ptr<UPClass> up2 = std::make_unique<UPClass>(2);
		up2->Print();

		// wykorzystanie unique_ptr do stworzenia 3 elementowej tablicy
		std::unique_ptr<UPClass[]> arr = std::make_unique<UPClass[]>(3);

		//up2 = up1;
		//***************************
		//  unique_ptr(const unique_ptr& Right) = delete;
		//	unique_ptr& operator=(const unique_ptr& Right) = delete;
		//***************************

		up2 = std::move(up1);
		up2->Print();
		//up1->Print(); //error, wskaznik nie wskazuje na instancje UPClass
	}

	std::unique_ptr<UPClass> p = std::make_unique<UPClass>();

	std::vector<std::unique_ptr<UPClass>> vec;

	// vec.push_back(p); // error
	// vector<unique_ptr<Animal>> vec2 = vec; // error

	// ok, ale p na nic nie wskazuje
	vec.push_back(std::move(p));

	// ok, ale dwa wskazniki pokazuja na to samo miejsce
	UPClass *p1 = p.get();

	// ok, ale p juz na nic nie wskazuje
	UPClass *p2 = p.release();
	std::cout << "KONIEC" << std::endl;
	delete p2;
}