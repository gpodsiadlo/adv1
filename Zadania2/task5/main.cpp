/* @autor: Grzegorz Podsiadło 
Zadanie polega na stworzeniu nierozszerzalnej klasy Enumerable 
symbolizującej obiekty prezentujące pewien zakres, po których można iterować.
Każdy obiekt tej klasy powinien umożliwiać opakowanie wybranego 
kontenera oraz udostępniać interfejs pozwalający na 
wykonywanie zapytań do sekwencji danych się w nim znajdujących.
Proszę zaimplementować poniższe operacje:

  where(calllable_predicate) -> umożliwia odsiewanie elementów korzystając z predykatu
  select(callable_mapper) -> umożliwia stworzenie nowej sekwencji mapując każdy jej element na inny, obsłużone mają być zmiany typów, na przykład int -> std::string powinien zwracać obiekt typu Enumerable bazujący na sekwencji std::string
  order_by() -> sortowanie
  order_by(callable_predicate) -> sortowanie według predykatu
  to_vector() -> zwrócenie wektora zawierającego wszystkie elementy w takiej samej kolejności, w jakiej iterujemy
  any() -> sprawdzenie czy są jakiekolwiek elementy
  any(callable_predicate) -> sprawdzenie czy jakikolwiek element spełnia predykat
  all(callable_predicate) ->  sprawdzenie czy wszystkie elementy spełniają określony predykat
  take(N) -> utworzenie sekwencji zawierającej max N elementów
  count() -> zwraca ilość elementów
  count(callable predicate) -> jw spełniających predykat
  to_vector() -> zwraca sekwencę w postaci std::vector

  from(container) -> zwraca obiekt enumerable<container_type>

UWAGA:
1.) Pliku main.cpp nie można modyfikować. 
2.) Kompilacja ze zdefiniowanym symbolem CHECK powinna kończyć się błędem.  
3.) Wewnętrzny sposób implementacji jest dowolny.
4.) O odpowiednie typy zwracane proszę zadbać używając nowych sposobów tworzenia funkcji.
5.) Po obiektach typu Enumerable powinna być dostepna iteracja z wykorzystaniem petli zakresowej for.
6.) Operacje na obiektach typu Enumerable nie powinny wpływać na obiekty tego typu oraz oryginalny kontener.
7.) Wszystko co nie jest powyżej zabronione/nakazane jest dozwolone.

CEL ZADANIA:
Celem zadania jest zapoznanie studentów z nowym sposobem kontroli dziedziczenia wprowadzonym 
wraz z nowym standardem języka. 
Dodatkowo zadanie ma na celu nauczyć wykorzystywać nowe sposoby definicji funkcji oraz tworzenie obiektów,
które można wykorzystywać do iterowania wewnątrz zakresowej pętli for.
*/

#include "LINQ.h"
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <deque>

#ifdef CHECK
class InheritanceCheck : LINQ::Enumerable<std::vector<int>>
{
};
#endif

int main()
{
  const std::vector<int> intVec = {1, 2, 3, 4, 5};

  std::cout << "TEST UŻYCIA PĘTLI ZAKRESOWEJ:" << std::endl;
  auto seq1 = LINQ::from(intVec);
  for (const auto &el : seq1)
    std::cout << el << " ";
  std::cout << std::endl;

  std::cout << "ODFILTROWANIE ELEMENTÓW WIĘKSZYCH OD 2 A NASTĘPNIE PODWOJENIE ICH:" << std::endl;
  const auto queryResult1 = LINQ::from(intVec)
                                .where([](auto a) { return a > 2; })
                                .select([](auto a) { return a * 2; })
                                .to_vector();

  for (const auto &el : queryResult1)
    std::cout << el << " ";
  std::cout << std::endl;

  std::cout << "ILOŚĆ ELEMENTÓW OGÓLNIE: " << seq1.count() << std::endl;
  std::cout << "ILOŚĆ ELEMENTÓW WIĘKSZYCH OD 3: " << seq1.count([](auto a) { return a > 3; }) << std::endl;
  std::cout << "CZY ZAWIERA ELEMENT WIĘKSZY OD 6: " << seq1.any([](auto a) { return a > 6; }) << std::endl;
  std::cout << "CZY WSZYSTKIE ELEMENTY SĄ DODATNIE: " << seq1.all([](auto a) { return a > 0; }) << std::endl;

  std::cout << "MAPOWANIE NA STRING:" << std::endl;
  const auto convertedToString = LINQ::from(intVec)
                                     .where([](auto a) { return a < 4; })
                                     .select([](auto a) { return std::string(">>") + std::to_string(a) + "<< converted to string\n"; })
                                     .to_vector();

  for (const auto &el : convertedToString)
    std::cout << el;

  std::cout << "ORYGINALNA KOLEKCJA:" << std::endl;
  for (const auto &el : intVec)
    std::cout << el << " ";
  std::cout << std::endl
            << std::endl
            << std::endl;

  /////////////////////////////////////////////////

  const std::list<double> doubleList = {100.0, 1032.5, 1023.1, 99.9, 44.0, 1.7};

  std::cout << "WYŁUSKANIE PIERWSZYCH 3 ELEMENTÓW" << std::endl;
  auto seq2 = LINQ::from(doubleList)
                  .take(3);

  for (const auto &el : seq2.to_vector())
    std::cout << el << " ";
  std::cout << std::endl;

  std::cout << "POSORTOWANIE ICH:" << std::endl;
  auto seq3 = seq2.order_by();

  for (const auto &el : seq3.to_vector())
    std::cout << el << " ";
  std::cout << std::endl;

  std::cout << "ORYGINALNA LISTA:" << std::endl;
  for (const auto &el : doubleList)
    std::cout << el << " ";
  std::cout << std::endl
            << std::endl
            << std::endl;

  /////////////////////////////////////////////////

  const std::deque<std::string> strDeq = {"Ala", "ma", "kota"};

  std::string prefix = ">> ";
  auto reverseSorted = LINQ::from(strDeq)
                           .order_by([](const auto &str1, const auto &str2) { return str2 < str1; })
                           .select([prefix](const auto &str) { return prefix + str; });

  for (const auto &el : reverseSorted)
    std::cout << el << std::endl;

  std::cout << "ORYGINALNA KOLEJKA:" << std::endl;
  for (const auto &el : strDeq)
    std::cout << el << " ";
  std::cout << std::endl;

  return 0;
}

//OUTPUT:
/*
TEST UŻYCIA PĘTLI ZAKRESOWEJ:
1 2 3 4 5
ODFILTROWANIE ELEMENTÓW WIĘKSZYCH OD 2 A NASTĘPNIE PODWOJENIE ICH:
6 8 10
ILOŚĆ ELEMENTÓW OGÓLNIE: 5
ILOŚĆ ELEMENTÓW WIĘKSZYCH OD 3: 2
CZY ZAWIERA ELEMENT WIĘKSZY OD 6: 0
CZY WSZYSTKIE ELEMENTY SĄ DODATNIE: 1
MAPOWANIE NA STRING:
>>1<< converted to string
>>2<< converted to string
>>3<< converted to string
ORYGINALNA KOLEKCJA:
1 2 3 4 5


WYŁUSKANIE PIERWSZYCH 3 ELEMENTÓW
100 1032.5 1023.1
POSORTOWANIE ICH:
100 1023.1 1032.5
ORYGINALNA LISTA:
100 1032.5 1023.1 99.9 44 1.7


>> ma
>> kota
>> Ala
ORYGINALNA KOLEJKA:
Ala ma kota
*/