#include <iostream>

struct A
{
  int i = 0;
  int &value() { return i; }
};
struct B
{
  int i = 0;
  int value() { return i; }
};

template <typename T>
auto foo(T &t) { return t.value(); }

template <typename T>
auto &bar(T &t) { return t.value(); }

template <typename T>
decltype(auto) foobar(T &t) { return t.value(); }

int main()
{
  A a;
  B b;
  //foo(a) = 20;
  foo(b);

  bar(a) = 20;
  //bar(b);

  foobar(a) = 20;
  foobar(b);
}