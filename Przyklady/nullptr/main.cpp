#include <iostream>
#include <cstddef>		 //nullptr_t
#include <type_traits> //is_null_pointer

void fun(int)
{
	std::cout << "Wersja int\n";
}
void fun(char *)
{
	std::cout << "Wersja char*\n";
}

int main()
{
	fun(static_cast<char *>(0));
	fun(0);
	// fun(NULL); //nie wiemy jak jest zdefiniowany (0 czy 0L)

	fun(nullptr);

	std::nullptr_t p;
	int val;
	std::cout << "Jest nullptr_t: " << std::is_null_pointer<decltype(p)>::value << std::endl;		// C++ 14
	std::cout << "Jest nullptr_t: " << std::is_null_pointer<decltype(val)>::value << std::endl; // C++ 14
}

template <typename T, T *ptr>
struct s
{
}; //podstawowy szablon klasy

template <>
struct s<nullptr_t, nullptr>
{
}; //specjalizacja dla nullptr