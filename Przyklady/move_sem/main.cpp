#include <iostream>
#include <utility>

class MyIntContainer
{
public:
	MyIntContainer(int n) : m_content(new int[n]{0}), m_n(n)
	{
		std::cout << "Tworzenie kontenera o poj: " << m_n << std::endl;
	}

	MyIntContainer(const MyIntContainer &source) : m_content(new int[source.m_n]), m_n(source.m_n)
	{
		std::cout << "Konstruktor kopiujacy" << std::endl;
		for (int i = 0; i < m_n; ++i)
			this->m_content[i] = source.m_content[i];
	}

	MyIntContainer(MyIntContainer &&source) : m_content(source.m_content), m_n(source.m_n)
	{
		std::cout << "Konstruktor przenoszacy" << std::endl;
		source.m_content = nullptr;
		source.m_n = 0;
	}

	MyIntContainer &operator=(MyIntContainer &&source)
	{
		std::cout << "Przenoszacy operator przypisania" << std::endl;
		if (this != &source)
		{
			delete[] m_content;

			m_content = source.m_content;
			m_n = source.m_n;
			source.m_content = nullptr;
			source.m_n = 0;
		}
		return *this;
	}

	~MyIntContainer()
	{
		std::cout << "Usuwanie kontenera" << std::endl;
		delete[] m_content;
	}

	MyIntContainer &set(int index, int val)
	{
		m_content[index] = val;
		return *this;
	}

	void print() const
	{
		for (int i = 0; i < m_n; i++)
		{
			std::cout << i << ": " << m_content[i] << std::endl;
		}
		std::cout << "\n";
	}

private:
	int *m_content;
	int m_n;
};

int main()
{
	MyIntContainer a(3);
	a.set(0, 10).set(1, 11).set(2, 40);
	a.print();

	MyIntContainer b(a); // konstruktor kopiujacy
	b.print();

	MyIntContainer c(std::move(b)); // konstruktor przenoszacy
	c.print();

	c = MyIntContainer(2); // przenoszacy operator przypisania
	c.print();

	return 0;
}