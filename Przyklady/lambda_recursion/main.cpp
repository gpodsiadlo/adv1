#include <iostream>
#include <functional>

int main()
{

  // auto lambda = [&lambda](const int depth) {
  //   std::cout << "Recursion depth: " << depth << std::endl;
  //   if (depth < 10)
  //     lambda(depth + 1);
  // };
  // lambda(0);

  std::function<void(int)> recursive_lambda = [&recursive_lambda](const int depth) {
    std::cout << "Recursion depth: " << depth << std::endl;
    if (depth < 10)
      recursive_lambda(depth + 1);
  };

  recursive_lambda(0);
  return 0;
}