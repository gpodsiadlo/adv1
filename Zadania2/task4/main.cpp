/* @autor: Grzegorz Podsiadło 
Proszę napisać wrapper tablicy, którego wartości wyznaczone są w czasie kompilacji oraz funkcję dwie funkcje:
a) pozwalającej sortować zdefiniowany literał, tak by po kompilacji wartości wewnątrz były w kolejnosci rosnącej
b) funkcję porównującą dwa literały tekstowe w czasie kompilacji

UWAGI:
1.) Pliku main.cpp nie można modyfikować. 
2.) Sortowanie powinno przebiegać w czasie kompilacji, wypisywanie w czasie działania programu.
3.) Zadanie ma przechodzić poprawną kompilację oraz nie powodować błędów pamieci.
4.) Zadanie powinno być wykonanezgodnie ze standardem  C++14.
5.) Implementacja sortowania jest dowolna, o ile dawać będzie odpowiedni rezultat. Niemniej jednak zadanie zostało przetestowane tylko dla najprostszych sortowań (bąbelkowe, wstawianie).
6.) Wszystko co nie jest powyżej zabronione/nakazane jest dozwolone.

CEL ZADANIA:
Zapoznanie ze zmianami wprowadzonymi w C++14 w kwesti constexpr. Przećwiczenie bardziej zaawansowanych technik stosowania funkcji, które moga być ewaluowane w czasie kompilacji.
*/

#include "main.h"
#include <iostream>

template <typename T, int N>
constexpr auto constexprCopyAndSort(const T (&tab)[N])
{
  Array<T, N> arr;

  for (int i{}; i < N; ++i)
    arr[i] = tab[i];

  sortImplementation(arr.begin(), arr.end(), std::less<T>());
  return arr;
}

template <int N>
constexpr auto constexprCopyAndSort(const char *const (&tab)[N])
{
  Array<const char *, N> arr;

  for (int i{}; i < N; ++i)
    arr[i] = tab[i];

  sortImplementation(arr.begin(), arr.end(), constexprStringCompare);
  return arr;
}

int main()
{

  // compile time
  constexpr const char *students[] = {"Flisikowski", "Laski", "Kowalski", "Abacki", "Iwanowicz", "Jankowski", "Kot"};
  constexpr auto arrStudents = constexprCopyAndSort(students);
  static_assert(arrStudents.size() == 7, "Wrong array size!");

  constexpr const int ints[] = {5, 3, 2, 1, -1, 5, 6};

  constexpr auto arrInts = constexprCopyAndSort(ints);
  static_assert(arrInts.size() == 7, "Wrong array size!");

  // runtime
  std::cout << "STUD:\n";
  for (int i{}; i < arrStudents.size(); ++i)
    std::cout << arrStudents[i] << std::endl;

  std::cout << "\nNUMBERS:\n";
  for (const auto &el : arrInts)
    std::cout << el << std::endl;

  return 0;
}

/*
STUD:
Abacki
Flisikowski
Iwanowicz
Jankowski
Kot
Kowalski
Laski

NUMBERS:
-1
1
2
3
5
5
6
*/