// Celem zadania jest napisanie klasy Kontroler.
// Kazda instancja klasy jest w stanie zbadac stan innych kontrolerow w dowolnym momencie.
// Wykorzystaj jeden z typow inteligentnych wskaznikow do zapewnienia prawidlowego usuwania
// obiektow, ktore maja zaleznosci cykliczne.
// Proszę nie modyfikować pliku main.cpp

// Autor D.K.

#include "Kontroler.h"

typedef std::vector<std::shared_ptr<Kontroler>> kontrolerWektor;

int main()
{
	kontrolerWektor baza;

	baza.push_back(std::shared_ptr<Kontroler>(new Kontroler(0)));
	baza.push_back(std::shared_ptr<Kontroler>(new Kontroler(1)));
	baza.push_back(std::shared_ptr<Kontroler>(new Kontroler(2)));
	baza.push_back(std::shared_ptr<Kontroler>(new Kontroler(3)));
	baza.push_back(std::shared_ptr<Kontroler>(new Kontroler(4)));

	std::cout << "\n";

	// Kazdy Kontroler jest zalezny od tego czy pozostale sa aktywne.
	// Nadajemy kazdemu Kontrolerowi wskazniki do pozostalych.
	for (unsigned index = 0; index < baza.size(); ++index)
	{
		for (kontrolerWektor::iterator it = baza.begin(); it != baza.end(); ++it)
		{
			if ((*it)->getId() != index)
			{
				baza[index]->dodajInny(*it);
				std::cout << "Dodano do baza[" << index << "]: " << (*it)->getId() << std::endl;
			}
		}
		std::cout << "\n";
	}

	for (kontrolerWektor::iterator it = baza.begin(); it != baza.end(); ++it)
	{
		if (it->use_count())
		{
			std::cout << "Sprawdzam status:\nKontroler " << (*it)->getId() << std::endl;
			(*it)->sprawdzStatus();
		}
	}
}

// Utworzono Kontroler o id = 0
// Utworzono Kontroler o id = 1
// Utworzono Kontroler o id = 2
// Utworzono Kontroler o id = 3
// Utworzono Kontroler o id = 4

// Dodano do baza[0]: 1
// Dodano do baza[0]: 2
// Dodano do baza[0]: 3
// Dodano do baza[0]: 4

// Dodano do baza[1]: 0
// Dodano do baza[1]: 2
// Dodano do baza[1]: 3
// Dodano do baza[1]: 4

// Dodano do baza[2]: 0
// Dodano do baza[2]: 1
// Dodano do baza[2]: 3
// Dodano do baza[2]: 4

// Dodano do baza[3]: 0
// Dodano do baza[3]: 1
// Dodano do baza[3]: 2
// Dodano do baza[3]: 4

// Dodano do baza[4]: 0
// Dodano do baza[4]: 1
// Dodano do baza[4]: 2
// Dodano do baza[4]: 3

// Sprawdzam status:
// Kontroler 0
// Status id = 1 - on
// Status id = 2 - on
// Status id = 3 - on
// Status id = 4 - on
// Sprawdzam status:
// Kontroler 1
// Status id = 0 - on
// Status id = 2 - on
// Status id = 3 - on
// Status id = 4 - on
// Sprawdzam status:
// Kontroler 2
// Status id = 0 - on
// Status id = 1 - on
// Status id = 3 - on
// Status id = 4 - on
// Sprawdzam status:
// Kontroler 3
// Status id = 0 - on
// Status id = 1 - on
// Status id = 2 - on
// Status id = 4 - on
// Sprawdzam status:
// Kontroler 4
// Status id = 0 - on
// Status id = 1 - on
// Status id = 2 - on
// Status id = 3 - on
// Zniszczono Kontroler o id = 0
// Zniszczono Kontroler o id = 1
// Zniszczono Kontroler o id = 2
// Zniszczono Kontroler o id = 3
// Zniszczono Kontroler o id = 4