#include <iostream>

class MakeFinal // klasa pomocnicza
{
    friend class Final;
    MakeFinal() { std::cout << "MakeFinal constructor" << std::endl; }
};

class Final : public virtual MakeFinal // wirtualna klasa bazowa, sprawia ze konstruktor makefinal wolany jest z konstrukotra derived, nie final
{
  public:
    Final() { std::cout << "Final constructor" << std::endl; }
};

class Derived
// : public Final // błąd kompilacji
{
  public:
    Derived() { std::cout << "Derived constructor" << std::endl; }
};

int main()
{
    Final f;
    Derived d;
    return 0;
}