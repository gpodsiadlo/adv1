#include <iostream>
#include <chrono>

using timePoint_t = decltype(std::chrono::steady_clock::now());

template <typename T>
using dt_good = std::chrono::duration<double, T>;

template <typename T>
struct dt_bad
{
  typedef std::chrono::duration<double, T> type;
};

/*
template<>
struct dt_bad<bool> {
  enum { type };
};
*/

template <typename T>
struct exeTimer
{
  dt_good<T> duration;
  timePoint_t lastStart;
  void start()
  {
    lastStart = std::chrono::steady_clock::now();
    std::cout << "time measure started" << std::endl;
  }
  void stop()
  {
    timePoint_t actual = std::chrono::steady_clock::now();
    duration = dt_good<T>(actual - lastStart);
    std::cout << "time measure ended: " << duration.count() << " elapsed" << std::endl;
  }
};

template <typename T>
struct exeTimerBad
{
  typename dt_bad<T>::type duration;
  timePoint_t lastStart;
  void start()
  {
    lastStart = std::chrono::steady_clock::now();
    std::cout << "time measure started" << std::endl;
  }
  void stop()
  {
    timePoint_t actual = std::chrono::steady_clock::now();
    duration = dt_good<T>(actual - lastStart);
    std::cout << "time measure ended: " << duration.count() << " elapsed" << std::endl;
  }
};

int main()
{
  exeTimer<std::milli> timer;
  timer.start();
  timer.stop();

  exeTimerBad<std::milli> timerBad;
  timerBad.start();
  timerBad.stop();

  return 0;
}