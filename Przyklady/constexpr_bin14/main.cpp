#include <iostream>
#include <stdexcept>

constexpr int binToInt(const char *string)
{
  int value = 0;
  for (int i{}; string[i]; ++i)
  {
    switch (string[i])
    {
    case ',':
      break;
    case '1':
      value = (value << 1) + 1;
      break;
    case '0':
      value = value << 1;
      break;
    default:
      throw std::logic_error("Only '0', '1', ',' allowed");
      break;
    }
  }

  return value;
}

int main()
{
  constexpr int i1 = binToInt("1");
  std::cout << "1b -> " << i1 << std::endl;

  constexpr int i3 = binToInt("11");
  std::cout << "11b -> " << i3 << std::endl;

  constexpr int i19 = binToInt("10011");
  std::cout << "10011b -> " << i19 << std::endl;

  constexpr int i178 = binToInt("1011,0010");
  std::cout << "1011,0011 -> " << i178 << std::endl;
}