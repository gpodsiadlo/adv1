#include <iostream>
#include <stdexcept>

int fib(const int i)
{
  if (i == 0)
    return 0;
  if (i == 1)
    return 1;
  return fib(i - 1) + fib(i - 2);
}

template <int in>
struct Fib
{
  static const int val = Fib<in - 1>::val + Fib<in - 2>::val;
};

template <>
struct Fib<0>
{
  static const int val = 0;
};

template <>
struct Fib<1>
{
  static const int val = 1;
};

constexpr int cfib(const int i)
{
  return i >= 0 ? (i < 2 ? i : (cfib(i - 1) + cfib(i - 2))) : throw std::logic_error("cfib: Negative input value!");
}

int main(int argc, char const *argv[])
{

  //std::cout << fib(45) << std::endl;

  //std::cout << Fib<45>::val << std::endl;
  //std::cout << Fib<argc>::val << std::endl;

  //constexpr int fib45 = cfib(45);
  //std::cout << fib45 << std::endl;

  //std::cout << cfib(45) << std::endl;

  //std::cout << argc << " fib: " << cfib(argc) << std::endl;

  //constexpr int fibNeg = cfib(-5);
  //std::cout << cfib(-5) << std::endl;

  return 0;
}
