#include <iostream>

struct Integer
{
  int m_int;
  constexpr Integer(const int i) : m_int{i} {}
  constexpr int const& get()  { return m_int; } // const w C++11 
  int& get() { return m_int; }
};

int main() {

  constexpr Integer i(10);
  Integer ii(10);
  ii.get() = 5;
  std::cout << ii.get() << std::endl;

}