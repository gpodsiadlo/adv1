static const char sc = 'c';

int main()
{

  constexpr auto ten = 10;
  constexpr auto c = 'c';
  constexpr auto uten = 10u;

  //constexpr char * pc = &c;
  constexpr const char *psc = &sc;

  constexpr double d = 10.0;
  constexpr float f = 10.0f;
  static_assert(ten == 10, "...");
  static_assert(*psc == 'c', "...");
}