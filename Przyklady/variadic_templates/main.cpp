﻿#include <iostream>
#include <vector>
#include <utility>

template<typename T>
void handle(T)
{
	std::cout << "GENERIC\n";
}

template<>
void handle(int arg)
{
	std::cout << "INT: " << arg << std::endl;
}

template<>
void handle(double arg)
{
	std::cout << "DOUBLE: " << arg << std::endl;
}

template<typename T>
void variadic_template(T arg)
{
	handle(arg);
}

template<typename T1, typename ... Ts>
void variadic_template(T1 arg, Ts... args)
{
	variadic_template(arg);
	variadic_template(args...);
}

//***************************************************************

int main()
{
	variadic_template(1, 1.2, "napis");
	std::cout << "\n";
	variadic_template(1, std::vector<int>());
	return 0;
}
