#include <iostream>
#include <type_traits>

class Point
{
  int m_x;
  int m_y;

public:
  constexpr Point() : m_x{}, m_y{} {}
  constexpr Point(int x, int y) : m_x{x}, m_y{y} {}
  constexpr int getX() const { return m_x; }
  constexpr int getY() const { return m_y; }
};

constexpr int get_five() { return 5; }
int main()
{

  constexpr Point p(5, 6);
  int i = p.getX();
  switch (i)
  {
  case get_five():
    std::cout << "five" << std::endl;
    break;
  case p.getY():
    std::cout << "six" << std::endl;
    break;
  default:
    std::cout << "not in {five, six}" << std::endl;
    break;
  }

  std::cout << std::boolalpha << std::is_trivially_destructible<Point>::value << std::endl;

  return 0;
}