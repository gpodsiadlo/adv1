﻿//Proszę napisać szablon klasy Container, który przechowuje wektor elementów typu T.
//Container bazuje zarówno na semantyce przenoszenia jak i perfect forwardingu. Posiada również
//interfejs umożliwiający użycie tej klasy w pętli for opartej na zakresie(begin(), end()).
//
//Drugą klasą do napisania jest Content przechowywujący wektor elementów typu int,
//obiekt typu string oraz unikalny wskaźnik unique_ptr. 
//
//Celem zadania jest zaznajomienie się w praktyce z mechanizmami perfect forwardingu oraz
//utrwalenie wiedzy z zakresu przenoszenia.
//Proszę nie zmieniać pliku main.cpp
//Autor: D.K.

#include "main.h"

int main()
{
	Container<Content> myCont;

	std::vector<int> ints = { 10, 20, 30 };
	std::string s = "Pierwszy napis";
	int* ptr = new int(5);

	myCont.AddObject(ints, s);
	
	ints.emplace_back(40);
	s += " + drugi napis";
	
	myCont.AddObject(std::move(ints), std::move(s), ptr);
	
	ints.emplace_back(50);
	ints.emplace_back(60);
	s += " + trzeci napis";
	myCont.AddObject(std::move(ints), std::move(s)).AddObject(std::vector<int>({ 70, 80 }), s + " + czwarty napis");;

	for (const Content& c : myCont)
		std::cout << c;

	Container<Content> newCont = std::move(myCont);

	std::cout << "\nmyCont:" << std::endl;
	for (const Content& c : myCont)
		std::cout << c;
	std::cout << "\nnewCont:" << std::endl;
	for (const Content& c : newCont)
		std::cout << c;

	return 0;
}

// Content::Content(std::vector<int>&, std::__cxx11::string&)
// Content::Content(std::vector<int>&&, std::__cxx11::string&&, int*)
// Content::Content(std::vector<int>&&, std::__cxx11::string&&)
// Content::Content(std::vector<int>&&, std::__cxx11::string&&)
// [ 10 20 30 ]    Pierwszy napis  <pusty wskaznik>
// [ 10 20 30 40 ] Pierwszy napis + drugi napis    5
// [ 50 60 ]        + trzeci napis <pusty wskaznik>
// [ 70 80 ]        + czwarty napis        <pusty wskaznik>

// myCont:

// newCont:
// [ 10 20 30 ]    Pierwszy napis  <pusty wskaznik>
// [ 10 20 30 40 ] Pierwszy napis + drugi napis    5
// [ 50 60 ]        + trzeci napis <pusty wskaznik>
// [ 70 80 ]        + czwarty napis        <pusty wskaznik>