/* @autor: Grzegorz Podsiadło 
Zadanie polega na napisaniu prostej klasy
opakowującej literały napisów znane z języka C.
Klasa ma umożliwiać użycie jej obiektów w funkcjach wyznaczających właściwości 
napisu w czasie kompilacji.

UWAGI:
1.) Pliku main.cpp nie można modyfikować. 
2.) Proszę nie używać żadnych dodatkowych bibliotek (w tym standardowych).
3.) Zadanie ma przechodzić poprawną kompilację oraz nie powodować błędów pamieci.
4.) Nie potrzebna jest obsługa znaków narodowych, wystarczy zwykłe ASCII.
5.) Zadanie ma być rozwiązane zgodnie ze standardem C++11, proszę nie zmieniać flag w makefile.
6.) Wszystko co nie jest powyżej zabronione/nakazane jest dozwolone.

CEL ZADANIA:
Zadanie ma na celu przećwiczenie przez studentów zastosowania nowego słowa \
kluczowego constexpr w standardzie C++11.
*/

#include "StringExtensions.h"

int main()
{
  constexpr ConstantString str1("Ala ma kota");
  constexpr ConstantString str2("");
  constexpr ConstantString str3("kajak");

  static_assert(str1.length() == 11, "String length is wrong!");
  static_assert(str2.length() == 0, "String length is wrong!");

  static_assert(str1[0] == 'A', "Wrong character returned!");
  static_assert(str1[10] == 'a', "Wrong character returned!");

  static_assert(isPalindrome(str1) == false, "Given string should not be a palindrome!");
  static_assert(isPalindrome(str3) == true, "Given string should be a palindrome!");
  static_assert(isPalindrome("aabbcc") == false, "Given string should be a palindrome!");

  static_assert(lowercaseCounter(str1) == 8, "Wrong number of lowercase letters in str1!");
  static_assert(lowercaseCounter(str2) == 0, "Wrong number of lowercase letters in str2!");
  static_assert(lowercaseCounter(str3) == 5, "Wrong number of lowercase letters in str3!");

  static_assert(uppercaseCounter(str1) == 1, "Wrong number of lowercase letters in str1!");
  static_assert(uppercaseCounter(str2) == 0, "Wrong number of lowercase letters in str2!");
  static_assert(uppercaseCounter(str3) == 0, "Wrong number of lowercase letters in str3!");

  static_assert(characterCounter(str1, 'a') == 3, "Number of characters 'a' in string should be different!");
  static_assert(characterCounter(str2, ' ') == 0, "Number of characters ' ' in string should be different!");
  static_assert(characterCounter(str3, 'j') == 1, "Number of characters 'j' in string should be different!");
  static_assert(characterCounter("test string", 't') == 3, "Number of characters 't' in string should be different!");

  return 0;
}